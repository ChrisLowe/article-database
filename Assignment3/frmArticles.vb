﻿Imports System.Data.OleDb
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Text.RegularExpressions   'To clean up URL's


Public Class frmArticles

    '************************************************************
    '               Controller
    '************************************************************

    Dim myOleDBConnection As OleDbConnection
    Dim myOleDBAdapter As OleDbDataAdapter
    Dim myDataSet As DataSet = New DataSet()
    Dim myDataTable As DataTable = New DataTable()
    Dim mySQLDataAdapter As SqlDataAdapter
    Dim myCommandBuilder As OleDbCommandBuilder
    Dim myConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=|DataDirectory|\AboutVB.accdb"
    Dim myTableName = "Articles"
    Dim editMode = False
    Dim editIndex As Integer
    Dim lastArticleTitle As String


    ' Removes the row with the given title
    Private Sub removeRowFromDatabase(ByVal aTitle As String)

        ' The delete SQL statement:
        '           DELETE FROM table_name
        '           WHERE(some_column = some_value)
        Dim deleteSQL = "DELETE FROM " & myTableName & " WHERE(artTitle = '" & aTitle & "')"
        Dim rowsChanged As Integer

        ' Update the database
        Try
            ' Open the data connection
            myOleDBConnection.Open()

            ' Create the SQL command
            Dim deleteSQLCommand As OleDbCommand
            deleteSQLCommand = New OleDbCommand(deleteSQL)

            ' Set the commands connection
            deleteSQLCommand.Connection = myOleDBConnection

            ' Execute the command
            rowsChanged = deleteSQLCommand.ExecuteNonQuery()

        Catch ex As Exception
            MessageBox.Show("Fatal Error. Could not delete the row with the statement: " & deleteSQL & ". Error is: " & ex.Message)
            Application.Exit()
        Finally
            ' Close the connection
            myOleDBConnection.Close()

            ' Report changes
            If (rowsChanged = 1) Then
                MessageBox.Show("Removed the article " & aTitle)
            ElseIf (rowsChanged > 1) Then
                MessageBox.Show("Removed " & rowsChanged & " articles")
            Else
                MessageBox.Show("Could not remove article " & aTitle)
            End If

        End Try

    End Sub


    Private Sub testUpdateDatabase()


        Dim sqlString = "UPDATE Articles SET artTitle='New Article' WHERE artKey=4;"
        Dim updateSQLCommand As New OleDbCommand(sqlString, myOleDBConnection)

        myOleDBConnection.Open()
        Dim rowsChanged = updateSQLCommand.ExecuteNonQuery
        myOleDBConnection.Close()

        MessageBox.Show("Rows changed: " & rowsChanged)

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        testUpdateDatabase()
    End Sub


    Private Sub updateRowToDatabase(ByVal dr As DataRow, ByVal idx As Integer)

        Dim updateSQLCommand As OleDbCommand
        Dim rowsChanged = 0

        ' Create the insert SQL statement
        Dim aKey = dr.Item("artKey")
        Dim aTitle = dr.Item("artTitle")
        Dim aDescription = dr.Item("artDescription")
        Dim aDate = dr.Item("artDate")
        Dim aVBNET = dr.Item("artVBNET")
        Dim aVB6 = dr.Item("artVB6")
        Dim aURL = dr.Item("artURL")

        ' Create the update statement
        'Update table_name SET column1=value, column2=value2,...
        'WHERE some_column = some_value


        'Dim sqlString = "UPDATE Articles SET artTitle='New Article'," & _
        '                "artDescription='None' " & __
        '                "WHERE artKey=1"

        '        " artDate = " & Date.Parse(aDate).ToShortDateString & ", " & _
        '" artVB6 = " & aVB6 & ", " & _
        '" artVBNET = " & aVBNET & ", " & _
        '" artURL = '" & aURL & "', " & _

        Dim updateSQLString = "UPDATE " & myTableName & _
            " SET artTitle='" & aTitle & "', " & _
            "artDescription='" & aDescription & "' " & _
            "WHERE artTitle='" & lastArticleTitle & "'"

        updateSQLCommand = New OleDbCommand(updateSQLString, myOleDBConnection)

        ' Update the database
        Try
            ' Open the data connection
            myOleDBConnection.Open()

            ' Execute the command
            rowsChanged = updateSQLCommand.ExecuteNonQuery()

        Catch ex As Exception
            MessageBox.Show("Fatal Error. Could not insert data with the statement: " & updateSQLCommand.CommandText & ". Error is: " & ex.Message)
            Application.Exit()
        Finally
            ' Close the connection
            myOleDBConnection.Close()

            ' Report changes
            If (rowsChanged = 1) Then
                MessageBox.Show("Updated the article " & aTitle & ", " & rowsChanged & " rows changed.")
            ElseIf (rowsChanged > 1) Then
                MessageBox.Show("Updated " & rowsChanged & " articles")
            Else
                MessageBox.Show("No changes where made with the statement: " & updateSQLString)
            End If


        End Try

    End Sub



    Private Sub addRowToDatabase(ByVal dr As DataRow)

        ' Create the insert SQL statement
        Dim aTitle = dr.Item("artTitle")
        Dim aDescription = dr.Item("artDescription")
        Dim aDate = dr.Item("artDate")
        Dim aVBNET = dr.Item("artVBNET")
        Dim aVB6 = dr.Item("artVB6")
        Dim aURL = dr.Item("artURL")

        'INSERT INTO table_name (col1, col2, col3) VALUES (value1, value2, value3,...)
        'Dim debugInsertSQL = "INSERT INTO Articles VALUES (" & (myDataTable.Rows.Count + 1) & ", 'Test', 22/2/03, true, false, 'none', 'test22')"
        Dim insertSQL = "INSERT INTO " & myTableName & "(" & _
        "artTitle, artDate, artVB6, artVBNET, artURL, artDescription)" & _
        " VALUES ('" & aTitle & "'," & aDate & "," & aVB6 & "," & aVBNET & ",'" & aURL & _
            "','" & aDescription & "')"

        ' Update the database
        Try
            ' Open the data connection
            myOleDBConnection.Open()

            ' Create the SQL command
            Dim insertCommand As OleDbCommand
            insertCommand = New OleDbCommand(insertSQL)

            ' Set the commands connection
            insertCommand.Connection = myOleDBConnection

            ' Execute the command
            insertCommand.ExecuteNonQuery()

        Catch ex As Exception
            MessageBox.Show("Fatal Error. Could not insert data with the statement: " & insertSQL & ". Error is: " & ex.Message)
            Application.Exit()
        Finally
            ' Close the connection
            myOleDBConnection.Close()
            MessageBox.Show("Added the article " & aTitle)
        End Try

    End Sub


    Private Sub openDatabase()


        myDataTable.Clear()


        Try
            'Create the OLEDB Connection
            myOleDBConnection = New OleDbConnection(myConnectionString)
            myOleDBConnection.Open()
        Catch
            MessageBox.Show("Fatal Error. Connection string incorrect.")
            Application.Exit()
        End Try

        'Populate the data table
        Try
            Dim mySQLString = "select * from " & myTableName
            myOleDBAdapter = New OleDbDataAdapter(mySQLString, myOleDBConnection)
            myOleDBAdapter.Fill(myDataTable)
        Catch
            MessageBox.Show("Fatal Error Opening " & myOleDBConnection.DataSource)
            Application.Exit()
        Finally
            myOleDBConnection.Close()
        End Try

        'Populate the pull down box
        fillArticleSelectionComboBox()

    End Sub


    '************************************************************
    '               Controller
    '************************************************************


    ' Fill the combo box with the title of all the articles in the database
    Private Sub fillArticleSelectionComboBox()
        cbxArticles.DataSource = myDataTable
        cbxArticles.DisplayMember = "artTitle"
        cbxArticles.ValueMember = "artKey"
    End Sub


    ' Updates the text boxes with the values at the give row
    Private Sub updateArticle(ByVal index As Integer)

        Try
            ' Use the column names to populate the form
            txtArticleTitle.Text = myDataTable.Rows(index).Item("artTitle")
            txtArticleDescription.Text = myDataTable.Rows(index).Item("artDescription")
            txtArticleDate.Text = myDataTable.Rows(index).Item("artDate")
            cbxArticleVB6.Checked = myDataTable.Rows(index).Item("artVB6")
            cbxArticleVBNET.Checked = myDataTable.Rows(index).Item("artVBNET")

            ' For some reason URL's are padded with #   - use regex to remove
            Dim webAddress = myDataTable.Rows(index).Item("artURL")
            Dim cleanAddress = Regex.Replace(webAddress, "#", "")
            txtArticleURL.Text = cleanAddress
        Catch ex As Exception
            ' MessageBox.Show("Could not open the article at index " & index)
        End Try

    End Sub

    ' Validate each of the form fields and create a datarow with the information
    ' When done, attempt to update the database
    Private Sub saveData()

        Dim dr As DataRow
        dr = myDataTable.NewRow

        If (validateText(txtArticleTitle.Text)) Then
            dr.Item("artTitle") = txtArticleTitle.Text
        Else
            MessageBox.Show("The title is not valid.")
            Exit Sub
        End If

        If (validateText(txtArticleDescription.Text)) Then
            dr.Item("artDescription") = txtArticleDescription.Text
        Else
            MessageBox.Show("The description is not valid.")
            Exit Sub
        End If

        If (validateText(txtArticleURL.Text)) Then
            dr.Item("artURL") = txtArticleURL.Text
        Else
            MessageBox.Show("The URL is not valid.")
            Exit Sub
        End If


        Dim d As Date
        Try
            d = Date.Parse(txtArticleDate.Text)
            dr.Item("artDate") = txtArticleDate.Text
        Catch ex As Exception
            MessageBox.Show("The date is not valid")
            Exit Sub
        End Try

        'If Not ((cbxArticleVB6.Checked) Or (cbxArticleVBNET.Checked)) Then
        '    MessageBox.Show("You must select one of the two check boxes")
        '    Exit Sub
        'End If

        dr.Item("artVB6") = cbxArticleVB6.Checked
        dr.Item("artVBNET") = cbxArticleVBNET.Checked
        dr.Item("artKey") = cbxArticles.SelectedIndex

        ' Add to the database
        addRowToDatabase(dr)

    End Sub



    Private Sub updateData()

        openDatabase()

        Dim dr As DataRow

        Dim aKey = myDataTable.Rows(cbxArticles.SelectedIndex).Item("artKey")


        dr = myDataTable.NewRow

        If (validateText(txtArticleTitle.Text)) Then
            dr.Item("artTitle") = txtArticleTitle.Text
        Else
            MessageBox.Show("The title is not valid.")
            Exit Sub
        End If

        If (validateText(txtArticleDescription.Text)) Then
            dr.Item("artDescription") = txtArticleDescription.Text
        Else
            MessageBox.Show("The description is not valid.")
            Exit Sub
        End If

        If (validateText(txtArticleURL.Text)) Then
            dr.Item("artURL") = txtArticleURL.Text
        Else
            MessageBox.Show("The URL is not valid.")
            Exit Sub
        End If


        Dim d As Date
        Try
            d = Date.Parse(txtArticleDate.Text)
            dr.Item("artDate") = txtArticleDate.Text
        Catch ex As Exception
            MessageBox.Show("The date is not valid")
            Exit Sub
        End Try

        'If Not ((cbxArticleVB6.Checked) Or (cbxArticleVBNET.Checked)) Then
        '    MessageBox.Show("You must select one of the two check boxes")
        '    Exit Sub
        'End If

        dr.Item("artVB6") = cbxArticleVB6.Checked
        dr.Item("artVBNET") = cbxArticleVBNET.Checked

        ' Add to the database

        updateRowToDatabase(dr, cbxArticles.SelectedIndex + 1)

    End Sub



    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        openDatabase()
        updateArticle(0)

        ' GUI toggles
        btnSave.Visible = False
        btnCancel.Visible = False
        btnSaveChanges.Visible = False
    End Sub


    Private Sub cbxArticles_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxArticles.SelectedIndexChanged
        editIndex = cbxArticles.SelectedIndex
        updateArticle(cbxArticles.SelectedIndex)
    End Sub


    '************************************************************
    '               View
    '************************************************************


    ' Opens the given string as a web address with the default browser
    Private Sub openURL(ByVal webAddress As String)

        ' Need to use regex to clean the address.. why?
        Dim url = Regex.Replace(webAddress, "#", "")

        Try
            System.Diagnostics.Process.Start(url)
        Catch ex As Exception
            Throw New ArgumentException("Could not open" & url)
        End Try
    End Sub


    ' Open button the article in the default web browser
    Private Sub btnOpen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnOpen.Click

        Dim webAddress = myDataTable.Rows(cbxArticles.SelectedIndex).Item("artURL")

        Try
            openURL(webAddress)
        Catch ex As Exception
            MessageBox.Show(ex.Message)  'clicking the button may cause problems
        End Try

    End Sub


    Private Sub txtArticleURL_MouseClick(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles txtArticleURL.MouseClick

        If Not (editMode) Then
            Dim webAddress = myDataTable.Rows(cbxArticles.SelectedIndex).Item("artURL")

            Try
                openURL(webAddress)
            Catch ex As Exception
                ' This could be perfectly normal. Such as editing the text box
            End Try
        End If


    End Sub




    Private Sub btnAdd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdd.Click

        ' Toggle to Add mode
        btnSave.Visible = True
        btnCancel.Visible = True
        btnAdd.Visible = False
        btnEdit.Visible = False
        btnDelete.Visible = False
        btnOpen.Enabled = False
        'btnPrevious.Enabled = False
        'btnNext.Enabled = False
        editMode = True

        ' Reset fields
        txtArticleDate.Text = ""
        txtArticleDescription.Text = ""
        txtArticleTitle.Text = ""
        txtArticleURL.Text = ""
        cbxArticleVB6.Checked = False
        cbxArticleVBNET.Checked = False
        cbxArticles.Enabled = True ''
    End Sub


    Private Sub btnCancel_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancel.Click

        editMode = False
        ' Toggle off Add mode
        cbxArticles.Enabled = True
        updateArticle(cbxArticles.SelectedIndex)
        btnSave.Visible = False
        btnCancel.Visible = False
        btnAdd.Visible = True
        btnEdit.Visible = True
        btnDelete.Visible = True
        btnOpen.Enabled = True
        btnNext.Enabled = True
        btnPrevious.Enabled = True
        btnSaveChanges.Visible = False
    End Sub


    Private Sub btnEdit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEdit.Click

        editMode = True
        lastArticleTitle = txtArticleTitle.Text
        btnAdd.Visible = False
        btnDelete.Visible = False
        btnNext.Enabled = False
        btnPrevious.Enabled = False
        btnOpen.Enabled = False
        btnSaveChanges.Visible = True
        btnEdit.Visible = False
        btnCancel.Visible = True

        editIndex = cbxArticles.SelectedIndex

        'cbxArticles.Enabled = False
    End Sub




    Private Sub btnSave_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click

        editMode = False
        saveData()
        btnSave.Visible = False
        btnCancel.Visible = False
        btnAdd.Visible = True
        btnDelete.Visible = True
        btnEdit.Visible = True
        btnOpen.Enabled = True
        btnNext.Enabled = True
        btnNext.Enabled = True
        cbxArticles.Enabled = True
        openDatabase()
        updateArticle(1)
    End Sub

    Private Sub btnSaveChanges_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSaveChanges.Click

        editMode = False
        updateData()
        btnSave.Visible = False
        btnAdd.Visible = True
        btnEdit.Visible = True
        btnDelete.Visible = True
        btnOpen.Enabled = True
        btnPrevious.Enabled = True
        btnNext.Enabled = True
        btnSaveChanges.Visible = False
        btnCancel.Visible = False
        openDatabase()
        updateArticle(1)
    End Sub


    Private Sub btnDelete_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDelete.Click

        Dim idx = cbxArticles.SelectedIndex
        Dim title = myDataTable.Rows(idx).Item("artTitle")
        removeRowFromDatabase(title)
        openDatabase()
        updateArticle(0)
    End Sub


    Private Sub btnPrevious_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPrevious.Click

        Dim idx = cbxArticles.SelectedIndex
        If (idx > 0) Then
            cbxArticles.SelectedIndex = idx - 1
            updateArticle(idx - 1)
        End If
    End Sub

    Private Sub btnNext_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNext.Click

        Try
            Dim idx = cbxArticles.SelectedIndex
            cbxArticles.SelectedIndex = idx + 1
            updateArticle(idx + 1)
        Catch ex As Exception
            'At the last item - don't do anything
        End Try
    End Sub


    Private Function validateText(ByVal testString As String) As Boolean
        If (testString.Length < 1) Then
            Return False
        End If
        Return True
    End Function


    Private Sub btnDebug_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDebug.Click
        txtArticleTitle.Text = "Test"
        txtArticleDescription.Text = "none"
        txtArticleURL.Text = "gggg"
        txtArticleDate.Text = Now().ToShortDateString
        cbxArticleVB6.Checked = True
    End Sub

    ' Do not allow changes to the text boxes unless in edit mode
    Private Sub txtArticleTitle_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtArticleTitle.KeyUp
        If Not (editMode) Then
            txtArticleTitle.Clear()
            updateArticle(cbxArticles.SelectedIndex)
        End If
    End Sub

    Private Sub txtArticleURL_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtArticleURL.KeyUp
        If Not (editMode) Then
            txtArticleURL.Clear()
            updateArticle(cbxArticles.SelectedIndex)
        End If
    End Sub

    Private Sub txtArticleDescription_KeyUp(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtArticleDescription.KeyUp
        If Not (editMode) Then
            txtArticleDescription.Clear()
            updateArticle(cbxArticles.SelectedIndex)
        End If
    End Sub

    Private Sub txtArticleDate_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtArticleDate.TextChanged
        If Not (editMode) Then
            txtArticleDate.Clear()
            updateArticle(cbxArticles.SelectedIndex)
        End If
    End Sub

    Private Sub cbxArticleVB6_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxArticleVB6.CheckedChanged
        If Not (editMode) Then
            updateArticle(cbxArticles.SelectedIndex)
        End If
    End Sub

    Private Sub cbxArticleVBNET_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxArticleVBNET.CheckedChanged
        If Not (editMode) Then
            updateArticle(cbxArticles.SelectedIndex)
        End If
    End Sub

   
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        MessageBox.Show(Environment.GetFolderPath(Environment.SpecialFolder.Favorites))
        ' System.Diagnostics.Process.Start("rundll32.exe", "shdocvw.dll,DoOrganizeFavDlg")
    End Sub
End Class




'From: http://msdn.microsoft.com/en-us/data/aa937722
' ADO.NET Manged Providers and DataSet......
'is the core data access technology for languages targeting the CLR. 
'Use the System.Data.SqlClient namespace to access SQL Server, 
'or providers from other suppliers to access their stores. 
'Use System.Data.Odbc or System.Data.Oledb to access data from 
'.NET languages using other data access technologies. 
'Use System.Data.Dataset when you need an offline data cache in client applications.
'Dim updateSQLString = "UPDATE " & myTableName & _
'    " SET artTitle=?," & _
'    " artDate=?, artVB6=?," & _
'    " artVBNET=?, artURL=?," & _
'    " artDescription=?" & _
'    " WHERE artKey=" & editIndex


'Dim updateSQLString = "UPDATE " & myTableName & _
'    " SET artTitle='" & aTitle & "', " & _
'        "artDate = " & aDate.ToString & ", " & _
'        "artVB6 = " & aVB6 & ", artVBNET = " & aVBNET & ", " & _
'        "artDescription = '" & aDescription & "' " &
'    " WHERE artKey = " & editIndex

'Command.Parameters.Add("Country", OleDbType.VarChar, 15)
'updateSQLCommand.CommandText = updateSQLString

'updateSQLCommand = New OleDbCommand(updateSQLString)
'updateSQLCommand.CommandType = CommandType.StoredProcedure

'Dim pTitle As New OleDbParameter("?", aTitle)
'pTitle.Value = aTitle
'pTitle.OleDbType = OleDbType.VarChar

'Dim pDate As New OleDbParameter("?", aDate)
'pDate.Value = aDate
'pDate.OleDbType = OleDbType.Date

'Dim pVBNet As New OleDbParameter("?", aVBNET)
'pVBNet.Value = aVBNET
'pVBNet.OleDbType = OleDbType.Boolean

'Dim pVB6 As New OleDbParameter("?", aVB6)
'pVB6.Value = aVB6
'pVB6.OleDbType = OleDbType.Boolean

'Dim pURL As OleDbParameter
'pURL = New OleDbParameter("?", aURL)
'pURL.Value = aURL
'pURL.OleDbType = OleDbType.VarChar

'Dim pDescription As OleDbParameter
'pDescription = New OleDbParameter("?", aDescription)
'pDescription.Value = aDescription
'pDescription.OleDbType = OleDbType.VarChar


'updateSQLCommand.Parameters.Add(pTitle)
'updateSQLCommand.Parameters.Add(pDate)
'updateSQLCommand.Parameters.Add(pVB6)
'updateSQLCommand.Parameters.Add(pVBNet)
'updateSQLCommand.Parameters.Add(pURL)
'updateSQLCommand.Parameters.Add(pDescription)


'Dim updateSQLString = "UPDATE " & myTableName & _
'            " SET (artTitle=?, artDate=?, artVB6=?, artVBNET=?, artURL=?, artDescription=?)" & _
'            " WHERE artKey = " & editIndex


'' updateSQLCommand = New OleDbCommand(updateSQLString, myOleDBConnection)
'updateSQLCommand = New OleDbCommand(updateSQLString, myOleDBConnection)

'updateSQLCommand.Parameters.Add("artTitle", OleDbType.VarChar).Value = aTitle
'updateSQLCommand.Parameters.Add("artDate", OleDbType.Date).Value = aDate
'updateSQLCommand.Parameters.Add("artVB6", OleDbType.Boolean).Value = aVB6
'updateSQLCommand.Parameters.Add("artVBNET", OleDbType.Boolean).Value = aVBNET
'updateSQLCommand.Parameters.Add("artURL", OleDbType.VarChar).Value = aURL
'updateSQLCommand.Parameters.Add("artDescription", OleDbType.VarChar).Value = aDescription
