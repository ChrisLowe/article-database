﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmArticles
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.cbxArticles = New System.Windows.Forms.ComboBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.btnOpen = New System.Windows.Forms.Button()
        Me.cbxArticleVB6 = New System.Windows.Forms.CheckBox()
        Me.cbxArticleVBNET = New System.Windows.Forms.CheckBox()
        Me.txtArticleDate = New System.Windows.Forms.RichTextBox()
        Me.txtArticleURL = New System.Windows.Forms.RichTextBox()
        Me.txtArticleTitle = New System.Windows.Forms.RichTextBox()
        Me.txtArticleDescription = New System.Windows.Forms.RichTextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btnSaveChanges = New System.Windows.Forms.Button()
        Me.btnCancel = New System.Windows.Forms.Button()
        Me.btnSave = New System.Windows.Forms.Button()
        Me.btnEdit = New System.Windows.Forms.Button()
        Me.btnNext = New System.Windows.Forms.Button()
        Me.btnPrevious = New System.Windows.Forms.Button()
        Me.btnAdd = New System.Windows.Forms.Button()
        Me.btnDelete = New System.Windows.Forms.Button()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.btnDebug = New System.Windows.Forms.Button()
        Me.ArticlesBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.AboutVBDataSet = New Assignment3.AboutVBDataSet()
        Me.ArticlesTableAdapter = New Assignment3.AboutVBDataSetTableAdapters.ArticlesTableAdapter()
        Me.AboutVBDataSet1 = New Assignment3.AboutVBDataSet()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.ArticlesBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AboutVBDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AboutVBDataSet1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'cbxArticles
        '
        Me.cbxArticles.FormattingEnabled = True
        Me.cbxArticles.Location = New System.Drawing.Point(95, 29)
        Me.cbxArticles.Name = "cbxArticles"
        Me.cbxArticles.Size = New System.Drawing.Size(408, 21)
        Me.cbxArticles.TabIndex = 2
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(17, 32)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(72, 13)
        Me.Label1.TabIndex = 4
        Me.Label1.Text = "Select Article:"
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.CadetBlue
        Me.GroupBox1.Controls.Add(Me.btnOpen)
        Me.GroupBox1.Controls.Add(Me.cbxArticleVB6)
        Me.GroupBox1.Controls.Add(Me.cbxArticleVBNET)
        Me.GroupBox1.Controls.Add(Me.txtArticleDate)
        Me.GroupBox1.Controls.Add(Me.txtArticleURL)
        Me.GroupBox1.Controls.Add(Me.txtArticleTitle)
        Me.GroupBox1.Controls.Add(Me.txtArticleDescription)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Location = New System.Drawing.Point(14, 62)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(529, 249)
        Me.GroupBox1.TabIndex = 5
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Article"
        '
        'btnOpen
        '
        Me.btnOpen.Location = New System.Drawing.Point(462, 68)
        Me.btnOpen.Name = "btnOpen"
        Me.btnOpen.Size = New System.Drawing.Size(49, 23)
        Me.btnOpen.TabIndex = 19
        Me.btnOpen.Text = "Open"
        Me.btnOpen.UseVisualStyleBackColor = True
        '
        'cbxArticleVB6
        '
        Me.cbxArticleVB6.AutoSize = True
        Me.cbxArticleVB6.Location = New System.Drawing.Point(305, 202)
        Me.cbxArticleVB6.Name = "cbxArticleVB6"
        Me.cbxArticleVB6.Size = New System.Drawing.Size(46, 17)
        Me.cbxArticleVB6.TabIndex = 18
        Me.cbxArticleVB6.Text = "VB6"
        Me.cbxArticleVB6.UseVisualStyleBackColor = True
        '
        'cbxArticleVBNET
        '
        Me.cbxArticleVBNET.AutoSize = True
        Me.cbxArticleVBNET.Location = New System.Drawing.Point(367, 201)
        Me.cbxArticleVBNET.Name = "cbxArticleVBNET"
        Me.cbxArticleVBNET.Size = New System.Drawing.Size(65, 17)
        Me.cbxArticleVBNET.TabIndex = 17
        Me.cbxArticleVBNET.Text = "VB.NET"
        Me.cbxArticleVBNET.UseVisualStyleBackColor = True
        '
        'txtArticleDate
        '
        Me.txtArticleDate.Location = New System.Drawing.Point(84, 199)
        Me.txtArticleDate.Name = "txtArticleDate"
        Me.txtArticleDate.Size = New System.Drawing.Size(187, 20)
        Me.txtArticleDate.TabIndex = 16
        Me.txtArticleDate.Text = ""
        '
        'txtArticleURL
        '
        Me.txtArticleURL.Location = New System.Drawing.Point(84, 67)
        Me.txtArticleURL.Name = "txtArticleURL"
        Me.txtArticleURL.Size = New System.Drawing.Size(372, 25)
        Me.txtArticleURL.TabIndex = 15
        Me.txtArticleURL.Text = ""
        '
        'txtArticleTitle
        '
        Me.txtArticleTitle.Location = New System.Drawing.Point(84, 29)
        Me.txtArticleTitle.Name = "txtArticleTitle"
        Me.txtArticleTitle.Size = New System.Drawing.Size(427, 25)
        Me.txtArticleTitle.TabIndex = 14
        Me.txtArticleTitle.Text = ""
        '
        'txtArticleDescription
        '
        Me.txtArticleDescription.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.txtArticleDescription.Location = New System.Drawing.Point(84, 105)
        Me.txtArticleDescription.Name = "txtArticleDescription"
        Me.txtArticleDescription.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None
        Me.txtArticleDescription.Size = New System.Drawing.Size(427, 78)
        Me.txtArticleDescription.TabIndex = 13
        Me.txtArticleDescription.Text = ""
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(37, 202)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(33, 13)
        Me.Label5.TabIndex = 12
        Me.Label5.Text = "Date:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(6, 107)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(63, 13)
        Me.Label4.TabIndex = 11
        Me.Label4.Text = "Description:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(37, 70)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(32, 13)
        Me.Label3.TabIndex = 10
        Me.Label3.Text = "URL:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(39, 32)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(30, 13)
        Me.Label2.TabIndex = 9
        Me.Label2.Text = "Title:"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btnSaveChanges)
        Me.GroupBox2.Controls.Add(Me.btnCancel)
        Me.GroupBox2.Controls.Add(Me.btnSave)
        Me.GroupBox2.Controls.Add(Me.Label1)
        Me.GroupBox2.Controls.Add(Me.cbxArticles)
        Me.GroupBox2.Controls.Add(Me.btnEdit)
        Me.GroupBox2.Controls.Add(Me.btnNext)
        Me.GroupBox2.Controls.Add(Me.btnPrevious)
        Me.GroupBox2.Controls.Add(Me.btnAdd)
        Me.GroupBox2.Controls.Add(Me.btnDelete)
        Me.GroupBox2.Location = New System.Drawing.Point(14, 326)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(529, 153)
        Me.GroupBox2.TabIndex = 6
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Navigation"
        '
        'btnSaveChanges
        '
        Me.btnSaveChanges.Location = New System.Drawing.Point(175, 73)
        Me.btnSaveChanges.Name = "btnSaveChanges"
        Me.btnSaveChanges.Size = New System.Drawing.Size(75, 55)
        Me.btnSaveChanges.TabIndex = 13
        Me.btnSaveChanges.Text = "Save Changes"
        Me.btnSaveChanges.UseVisualStyleBackColor = True
        '
        'btnCancel
        '
        Me.btnCancel.Location = New System.Drawing.Point(276, 73)
        Me.btnCancel.Name = "btnCancel"
        Me.btnCancel.Size = New System.Drawing.Size(75, 55)
        Me.btnCancel.TabIndex = 6
        Me.btnCancel.Text = "Cancel"
        Me.btnCancel.UseVisualStyleBackColor = True
        '
        'btnSave
        '
        Me.btnSave.Location = New System.Drawing.Point(175, 73)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(75, 55)
        Me.btnSave.TabIndex = 7
        Me.btnSave.Text = "Save"
        Me.btnSave.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Location = New System.Drawing.Point(229, 72)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(75, 56)
        Me.btnEdit.TabIndex = 12
        Me.btnEdit.Text = "Edit"
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'btnNext
        '
        Me.btnNext.Location = New System.Drawing.Point(381, 72)
        Me.btnNext.Name = "btnNext"
        Me.btnNext.Size = New System.Drawing.Size(75, 56)
        Me.btnNext.TabIndex = 11
        Me.btnNext.Text = "Next -->"
        Me.btnNext.UseVisualStyleBackColor = True
        '
        'btnPrevious
        '
        Me.btnPrevious.Location = New System.Drawing.Point(73, 72)
        Me.btnPrevious.Name = "btnPrevious"
        Me.btnPrevious.Size = New System.Drawing.Size(75, 56)
        Me.btnPrevious.TabIndex = 10
        Me.btnPrevious.Text = "<-- Previous"
        Me.btnPrevious.UseVisualStyleBackColor = True
        '
        'btnAdd
        '
        Me.btnAdd.Location = New System.Drawing.Point(154, 72)
        Me.btnAdd.Name = "btnAdd"
        Me.btnAdd.Size = New System.Drawing.Size(69, 56)
        Me.btnAdd.TabIndex = 5
        Me.btnAdd.Text = "New"
        Me.btnAdd.UseVisualStyleBackColor = True
        '
        'btnDelete
        '
        Me.btnDelete.Location = New System.Drawing.Point(310, 72)
        Me.btnDelete.Name = "btnDelete"
        Me.btnDelete.Size = New System.Drawing.Size(65, 56)
        Me.btnDelete.TabIndex = 9
        Me.btnDelete.Text = "Delete"
        Me.btnDelete.UseVisualStyleBackColor = True
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 27.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.Color.Navy
        Me.Label6.Location = New System.Drawing.Point(166, 9)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(248, 42)
        Me.Label6.TabIndex = 7
        Me.Label6.Text = ".NET Articles"
        '
        'btnDebug
        '
        Me.btnDebug.Location = New System.Drawing.Point(12, 485)
        Me.btnDebug.Name = "btnDebug"
        Me.btnDebug.Size = New System.Drawing.Size(75, 23)
        Me.btnDebug.TabIndex = 8
        Me.btnDebug.Text = "Debug"
        Me.btnDebug.UseVisualStyleBackColor = True
        '
        'ArticlesBindingSource
        '
        Me.ArticlesBindingSource.DataMember = "Articles"
        Me.ArticlesBindingSource.DataSource = Me.AboutVBDataSet
        '
        'AboutVBDataSet
        '
        Me.AboutVBDataSet.DataSetName = "AboutVBDataSet"
        Me.AboutVBDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ArticlesTableAdapter
        '
        Me.ArticlesTableAdapter.ClearBeforeFill = True
        '
        'AboutVBDataSet1
        '
        Me.AboutVBDataSet1.DataSetName = "AboutVBDataSet"
        Me.AboutVBDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(6, 415)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 14
        Me.Button1.Text = "Button1"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(189, 517)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 15
        Me.Button2.Text = "Button2"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'frmArticles
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.CadetBlue
        Me.ClientSize = New System.Drawing.Size(557, 552)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.btnDebug)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "frmArticles"
        Me.Text = ".NET Articles"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.ArticlesBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AboutVBDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AboutVBDataSet1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents AboutVBDataSet As Assignment3.AboutVBDataSet
    Friend WithEvents ArticlesBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ArticlesTableAdapter As Assignment3.AboutVBDataSetTableAdapters.ArticlesTableAdapter
    Friend WithEvents AboutVBDataSet1 As Assignment3.AboutVBDataSet
    Friend WithEvents cbxArticles As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents txtArticleDescription As System.Windows.Forms.RichTextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtArticleURL As System.Windows.Forms.RichTextBox
    Friend WithEvents txtArticleTitle As System.Windows.Forms.RichTextBox
    Friend WithEvents cbxArticleVB6 As System.Windows.Forms.CheckBox
    Friend WithEvents cbxArticleVBNET As System.Windows.Forms.CheckBox
    Friend WithEvents txtArticleDate As System.Windows.Forms.RichTextBox
    Friend WithEvents btnOpen As System.Windows.Forms.Button
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents btnAdd As System.Windows.Forms.Button
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents btnCancel As System.Windows.Forms.Button
    Friend WithEvents btnDebug As System.Windows.Forms.Button
    Friend WithEvents btnDelete As System.Windows.Forms.Button
    Friend WithEvents btnEdit As System.Windows.Forms.Button
    Protected WithEvents btnNext As System.Windows.Forms.Button
    Friend WithEvents btnPrevious As System.Windows.Forms.Button
    Friend WithEvents btnSaveChanges As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button

End Class
